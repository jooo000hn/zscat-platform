package com.zscat.platform.system.controller;

import com.zscat.common.utils.Query;
import com.zscat.common.utils.R;
import com.zscat.platform.common.annotation.Log;
import com.zscat.platform.common.controller.BaseController;
import com.zscat.platform.system.domain.RoleDO;
import com.zscat.platform.system.service.RoleService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RequestMapping("/sys/role")
@Controller
public class RoleController extends BaseController {
    String prefix = "system/role";
    @Autowired
    RoleService roleService;

    @ResponseBody
    @GetMapping("/list")
    R list(@RequestParam Map<String, Object> params) {
        R r = new R();
        // 查询列表数据
        Query query = new Query(params);
        List<RoleDO> sysDeptList = roleService.list(query);
        int total = roleService.count(query);
        r.put("rows", sysDeptList);
        r.put("total", total);
        return r;
    }


    @ResponseBody
    @GetMapping("/detail")
    R detail(Model model, Long roleId) {
        R r = new R();
        RoleDO sysDept = roleService.get(roleId);
        r.put("entity", sysDept);
        return r;
    }

    /**
     * 保存
     */
    @ResponseBody
    @PostMapping("/save")
    public R save(@RequestBody RoleDO sysDept) {
        if ("test".equals(getUsername())) {
            return R.error(1, "演示系统不允许修改,完整体验请部署程序");
        }
        if (sysDept.getRoleId() == null) {
            if (roleService.save(sysDept) > 0) {
                return R.ok();
            }
        } else {
            if (roleService.update(sysDept) > 0) {
                return R.ok();
            }
        }
        return R.error();
    }

    @Log("更新角色")
    // // @RequiresPermissions("sys:role:edit")
    @PostMapping("/update")
    @ResponseBody()
    R update(RoleDO role) {
        if ("test".equals(getUsername())) {
            return R.error(1, "演示系统不允许修改,完整体验请部署程序");
        }
        if (roleService.update(role) > 0) {
            return R.ok();
        } else {
            return R.error(1, "保存失败");
        }
    }

    @Log("删除角色")
    // // @RequiresPermissions("sys:role:remove")
    @PostMapping("/remove")
    @ResponseBody()
    R save(Long id) {
        if ("test".equals(getUsername())) {
            return R.error(1, "演示系统不允许修改,完整体验请部署程序");
        }
        if (roleService.remove(id) > 0) {
            return R.ok();
        } else {
            return R.error(1, "删除失败");
        }
    }

    // // @RequiresPermissions("sys:role:batchRemove")

    /**
     * 批量删除
     */
    @PostMapping("/batchRemove")
    @ResponseBody
    public R remove(String roleIds) {
        if (StringUtils.isBlank(roleIds)) {
            return R.error();
        }
        String[] ids = roleIds.split(",");
        if ("test".equals(getUsername())) {
            return R.error(1, "演示系统不允许修改,完整体验请部署程序");
        }
        roleService.batchremove(ids);
        return R.ok();
    }
}
