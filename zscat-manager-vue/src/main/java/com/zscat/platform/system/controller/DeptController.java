package com.zscat.platform.system.controller;

import com.zscat.common.utils.Query;
import com.zscat.common.utils.R;
import com.zscat.common.utils.Tree;
import com.zscat.platform.common.controller.BaseController;
import com.zscat.platform.common.excel.ExcelData;
import com.zscat.platform.common.excel.ExportExcelUtils;
import com.zscat.platform.system.domain.DeptDO;
import com.zscat.platform.system.service.DeptService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 部门管理
 *
 * @author chglee
 * @email 1992lcg@163.com
 * @date 2017-09-27 14:40:36
 */

@Controller
@RequestMapping("/sys/dep")
public class DeptController extends BaseController {
    @Autowired
    private DeptService sysDeptService;


    @ResponseBody
    @GetMapping("/list")
    R list(@RequestParam Map<String, Object> params) {
        R r = new R();
        // 查询列表数据
        Query query = new Query(params);
        List<DeptDO> sysDeptList = sysDeptService.list(query);
        int total = sysDeptService.count(query);
        r.put("rows", sysDeptList);
        r.put("total", total);
        return r;
    }


    @ResponseBody
    @GetMapping("/detail")
    R detail(Model model, Long deptId) {
        R r = new R();
        DeptDO sysDept = sysDeptService.get(deptId);
        r.put("entity", sysDept);
        return r;
    }

    /**
     * 保存
     */
    @ResponseBody
    @PostMapping("/save")
    public R save(@RequestBody DeptDO sysDept) {
        if ("test".equals(getUsername())) {
            return R.error(1, "演示系统不允许修改,完整体验请部署程序");
        }
        if (sysDept.getDeptId() == null) {
            if (sysDeptService.save(sysDept) > 0) {
                return R.ok();
            }
        } else {
            if (sysDeptService.update(sysDept) > 0) {
                return R.ok();
            }
        }
        return R.error();
    }


    /**
     * 删除
     */
    @PostMapping("/remove")
    @ResponseBody
    public R remove(Long deptId) {
        if ("test".equals(getUsername())) {
            return R.error(1, "演示系统不允许修改,完整体验请部署程序");
        }
        if (sysDeptService.remove(deptId) > 0) {
            return R.ok();
        }
        return R.error();
    }

    /**
     * 批量删除
     */
    @PostMapping("/batchRemove")
    @ResponseBody
    public R remove(String deptIds) {
        if (StringUtils.isBlank(deptIds)) {
            return R.error();
        }
        String[] ids = deptIds.split(",");
        if ("test".equals(getUsername())) {
            return R.error(1, "演示系统不允许修改,完整体验请部署程序");
        }
        sysDeptService.batchRemove(ids);
        return R.ok();
    }

    @RequestMapping(value = "/exportDep", method = RequestMethod.GET)
    public void excel(HttpServletResponse response, @RequestParam Map<String, Object> params) throws Exception {
        ExcelData data = new ExcelData();
        data.setName("部门列表");
        List<String> titles = new ArrayList();
        titles.add("a1");
        titles.add("a2");
        titles.add("a3");
        data.setTitles(titles);
        Query query = new Query(params);
        List<DeptDO> rows = sysDeptService.list(query);


        //生成本地
        /*File f = new File("c:/test.xlsx");
        FileOutputStream out = new FileOutputStream(f);
        ExportExcelUtils.exportExcel(data, out);
        out.close();*/
        ExportExcelUtils.exportExcel(response, "hello.xlsx", data);
    }

    @GetMapping("/tree")
    @ResponseBody
    public Tree<DeptDO> tree() {
        Tree<DeptDO> tree = new Tree<DeptDO>();
        tree = sysDeptService.getTree();
        return tree;
    }


}
