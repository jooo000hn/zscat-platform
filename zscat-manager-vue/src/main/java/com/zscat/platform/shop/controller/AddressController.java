package com.zscat.platform.shop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zscat.common.utils.Query;
import com.zscat.common.utils.R;
import com.zscat.goods.entity.AddressDO;
import com.zscat.goods.service.AddressService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author zscat
 * @email 951449465@qq.com
 * @date 2017-10-20 11:41:11
 */

@Controller
@RequestMapping("/shop/address")
public class AddressController {
    @Reference(
            version = "${web.service.version}",
            application = "${dubbo.application.id}",
            registry = "${dubbo.registry.id}"
    )
    private AddressService addressService;

    @ResponseBody
    @GetMapping("/list")
    R list(@RequestParam Map<String, Object> params) {
        R r = new R();
        // 查询列表数据
        Query query = new Query(params);
        List<AddressDO> sysDeptList = addressService.list(query);
        int total = addressService.count(query);
        r.put("rows", sysDeptList);
        r.put("total", total);
        return r;
    }


    @ResponseBody
    @GetMapping("/detail")
    R detail(Model model, Long id) {
        R r = new R();
        AddressDO sysDept = addressService.get(id);
        r.put("entity", sysDept);
        return r;
    }

    /**
     * 保存
     */
    @ResponseBody
    @PostMapping("/save")
    public R save(@RequestBody AddressDO sysDept) {

        if (sysDept.getId() == null) {
            if (addressService.save(sysDept) > 0) {
                return R.ok();
            }
        } else {
            if (addressService.update(sysDept) > 0) {
                return R.ok();
            }
        }
        return R.error();
    }
    /**
     * 修改
     */
    @ResponseBody
    @RequestMapping("/update")
    // @RequiresPermissions("shop:address:edit")
    public R update(AddressDO address) {
        addressService.update(address);
        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/remove")
    @ResponseBody
    // @RequiresPermissions("shop:address:remove")
    public R remove(Long id) {
        if (addressService.remove(id) > 0) {
            return R.ok();
        }
        return R.error();
    }

    /**
     * 删除
     */
    @PostMapping("/batchRemove")
    @ResponseBody
    // @RequiresPermissions("shop:address:batchRemove")
    public R remove(@RequestParam("ids[]") Long[] ids) {
        addressService.batchRemove(ids);
        return R.ok();
    }

}
