package com.zscat.platform.shop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zscat.common.utils.Query;
import com.zscat.common.utils.R;
import com.zscat.user.entity.FavoriteDO;
import com.zscat.user.service.FavoriteService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


/**
 * @author zscat
 * @email 951449465@qq.com
 * @date 2017-10-21 11:21:45
 */

@Controller
@RequestMapping("/shop/favorite")
public class FavoriteController {
    @Reference(
            version = "${web.service.version}",
            application = "${dubbo.application.id}",
            registry = "${dubbo.registry.id}"
    )
    private FavoriteService favoriteService;

    /**
     * 列表
     *
     * @param params
     * @return
     */
    @ResponseBody
    @GetMapping("/list")
    R list(@RequestParam Map<String, Object> params) {
        R r = new R();
        // 查询列表数据
        Query query = new Query(params);
        List<FavoriteDO> list = favoriteService.list(query);
        int total = favoriteService.count(query);
        r.put("rows", list);
        r.put("total", total);
        return r;
    }

    /**
     * 详情
     *
     * @param model
     * @param id
     * @return
     */
    @ResponseBody
    @GetMapping("/detail")
    R detail(Model model, Long id) {
        R r = new R();
        FavoriteDO sysDept = favoriteService.get(id);
        r.put("entity", sysDept);
        return r;
    }

    /**
     * 保存
     */
    @ResponseBody
    @PostMapping("/save")
    public R save(@RequestBody FavoriteDO entity) {

        if (entity.getId() == null) {
            if (favoriteService.save(entity) > 0) {
                return R.ok();
            }
        } else {
            if (favoriteService.update(entity) > 0) {
                return R.ok();
            }
        }
        return R.error();
    }
    /**
     * 修改
     */
    @ResponseBody
    @RequestMapping("/update")
    // @RequiresPermissions("shop:favorite:edit")
    public R update(FavoriteDO favorite) {
        favoriteService.update(favorite);
        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/remove")
    @ResponseBody
    // @RequiresPermissions("shop:favorite:remove")
    public R remove(Long id) {
        if (favoriteService.remove(id) > 0) {
            return R.ok();
        }
        return R.error();
    }

    /**
     * 删除
     */
    @PostMapping("/batchRemove")
    @ResponseBody
    // @RequiresPermissions("shop:favorite:batchRemove")
    public R remove(@RequestParam("ids[]") Long[] ids) {
        favoriteService.batchRemove(ids);
        return R.ok();
    }

}
