package com.zscat.platform.shop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zscat.common.utils.Query;
import com.zscat.common.utils.R;
import com.zscat.goods.entity.BannerDO;
import com.zscat.goods.service.BannerService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


/**
 * @author zscat
 * @email 951449465@qq.com
 * @date 2017-10-18 10:31:32
 */

@Controller
@RequestMapping("/shop/banner")
public class BannerController {
    @Reference(
            version = "${web.service.version}",
            application = "${dubbo.application.id}",
            registry = "${dubbo.registry.id}"
    )
    private BannerService bannerService;

    @ResponseBody
    @GetMapping("/list")
    R list(@RequestParam Map<String, Object> params) {
        R r = new R();
        // 查询列表数据
        Query query = new Query(params);
        List<BannerDO> list = bannerService.list(query);
        int total = bannerService.count(query);
        r.put("rows", list);
        r.put("total", total);
        return r;
    }


    @ResponseBody
    @GetMapping("/detail")
    R detail(Model model, Long id) {
        R r = new R();
        BannerDO sysDept = bannerService.get(id);
        r.put("entity", sysDept);
        return r;
    }

    /**
     * 保存
     */
    @ResponseBody
    @PostMapping("/save")
    public R save(@RequestBody BannerDO sysDept) {

        if (sysDept.getId() == null) {
            if (bannerService.save(sysDept) > 0) {
                return R.ok();
            }
        } else {
            if (bannerService.update(sysDept) > 0) {
                return R.ok();
            }
        }
        return R.error();
    }
    /**
     * 修改
     */
    @ResponseBody
    @RequestMapping("/update")
    // @RequiresPermissions("shop:banner:edit")
    public R update(BannerDO banner) {
        bannerService.update(banner);
        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/remove")
    @ResponseBody
    // @RequiresPermissions("shop:banner:remove")
    public R remove(Long id) {
        if (bannerService.remove(id) > 0) {
            return R.ok();
        }
        return R.error();
    }

    /**
     * 删除
     */
    @PostMapping("/batchRemove")
    @ResponseBody
    // @RequiresPermissions("shop:banner:batchRemove")
    public R remove(@RequestParam("ids[]") Long[] ids) {
        bannerService.batchRemove(ids);
        return R.ok();
    }

}
