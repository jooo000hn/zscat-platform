package com.zscat.platform.shop.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zscat.common.utils.Query;
import com.zscat.common.utils.R;
import com.zscat.goods.entity.TBrandDO;
import com.zscat.goods.service.TBrandService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author zscat
 * @email 951449465@qq.com
 * @date 2017-10-15 15:07:36
 */

@Controller
@RequestMapping("/shop/tBrand")
public class TBrandController {
    @Reference(
            version = "${web.service.version}",
            application = "${dubbo.application.id}",
            registry = "${dubbo.registry.id}"
    )
    private TBrandService tBrandService;

    /**
     * 列表
     *
     * @param params
     * @return
     */
    @ResponseBody
    @GetMapping("/list")
    R list(@RequestParam Map<String, Object> params) {
        R r = new R();
        // 查询列表数据
        Query query = new Query(params);
        List<TBrandDO> list = tBrandService.list(query);
        int total = tBrandService.count(query);
        r.put("rows", list);
        r.put("total", total);
        return r;
    }

    /**
     * 详情
     *
     * @param model
     * @param id
     * @return
     */
    @ResponseBody
    @GetMapping("/detail")
    R detail(Model model, Long id) {
        R r = new R();
        TBrandDO sysDept = tBrandService.get(id);
        r.put("entity", sysDept);
        return r;
    }

    /**
     * 保存
     */
    @ResponseBody
    @PostMapping("/save")
    public R save(@RequestBody TBrandDO entity) {

        if (entity.getId() == null) {
            if (tBrandService.save(entity) > 0) {
                return R.ok();
            }
        } else {
            if (tBrandService.update(entity) > 0) {
                return R.ok();
            }
        }
        return R.error();
    }
    /**
     * 修改
     */
    @ResponseBody
    @RequestMapping("/update")
    // @RequiresPermissions("shop:tBrand:edit")
    public R update(TBrandDO tBrand) {
        tBrandService.update(tBrand);
        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/remove")
    @ResponseBody
    // @RequiresPermissions("shop:tBrand:remove")
    public R remove(Long id) {
        if (tBrandService.remove(id) > 0) {
            return R.ok();
        }
        return R.error();
    }

    /**
     * 删除
     */
    @PostMapping("/batchRemove")
    @ResponseBody
    // @RequiresPermissions("shop:tBrand:batchRemove")
    public R remove(@RequestParam("ids[]") Long[] ids) {
        tBrandService.batchRemove(ids);
        return R.ok();
    }

}
